#ifndef TANK_H
#define TANK_H

#include "Missile.h"
#include <vector>
#include "TankConfiguration.h"

class Tank;

extern std::vector<Missile> missiles;		// wektor aktualnie lecacych pociskow
extern std::vector<Tank> tanks;		// wektor czolgow znajdujacych sie na planszy
extern PlayerTank playerTank;
extern GLuint textures[TEXTURES_NO];

class Tank
{
protected:
	float x, z;
	int hp;
	int direction;
	int reloading;	// przeladowywanie broni
	int trackNo;	// potrzebne do odrysowywania tekstury gasienicy
public:
	Tank();
	Tank(float x, float z, int direction);
	~Tank();
	void move();
	void fire();
	float getX();
	float getZ();
	int getDirection();
	void getHitted();
	int getHp();
	void draw();
private:
	int movementLong;	// zmienna mowi jak dlugo czolg porusza sie w jednym kierunku
	bool dead;		// czy jest juz martwy
	void drawAlive();
	void drawDead();
	void erase();
};


#endif