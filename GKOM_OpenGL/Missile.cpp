#include "Missile.h"
#include "Tank.h"
#include "PlayerTank.h"
#include "TankConfiguration.h"

extern std::vector<Missile> missiles;

Missile::Missile()
{
}


Missile::~Missile()
{
}

Missile::Missile(float x, float z, int direction)
{
	this->x = x;
	this->z = z;
	this->direction = direction;

	int sections = 10;
	int splices = 5;

	s_MissileVerticesCount = (sections + 2) * splices;

	s_MissileVertices = new GLfloat[s_MissileVerticesCount * 3];
	s_MissileNormals = new GLfloat[s_MissileVerticesCount * 3];
	s_MissileIndicesCount = (sections + 1) * (splices - 1) * 4;
	s_MissileIndices = new GLushort[s_MissileIndicesCount];

	int offset = 0;
	for (int section = 0; section < sections + 2; section++)
	{
		float angleSection = section / (float)sections * 2.0f * 3.14f;
		for (int splice = 0; splice < splices; splice++)
		{
			float angleSplice = splice / (float)(splices - 1) * 3.14f / 2;
			s_MissileVertices[offset + 0] = 0.05 * sin(angleSection) * cos(angleSplice);
			s_MissileVertices[offset + 1] = 0.05 * -sin(angleSplice);
			s_MissileVertices[offset + 2] = 0.05 * cos(angleSection) * cos(angleSplice);
			offset += 3;
		}
	}
	offset = 0;
	for (int section = 0; section < sections + 1; section++)
	{
		for (int splice = 0; splice < splices - 1; splice++)
		{
			s_MissileIndices[offset + 0] = section * splices + splice;
			s_MissileIndices[offset + 1] = section * splices + splice + 1;
			s_MissileIndices[offset + 2] = (section + 1) * splices + splice + 1;
			s_MissileIndices[offset + 3] = (section + 1) * splices + splice;

			offset += 4;
		}
	}
	BruteForceNormalCalculation(s_MissileVertices, s_MissileVerticesCount, s_MissileIndices, s_MissileIndicesCount, s_MissileNormals);
}

void Missile::move()
{
	switch (direction)
	{
	case 0:
		x += MISSILE_SPEED;
		break;
	case 1:
		z -= MISSILE_SPEED;
		break;
	case 2:
		x -= MISSILE_SPEED;
		break;
	case 3:
		z += MISSILE_SPEED;
		break;
	}
	if (x + MISSILE_LENGTH / 2 > BOARD_SIZE / 2 || x - MISSILE_LENGTH / 2 < -BOARD_SIZE / 2		// sprawdzamy czy pocisk nie wylecial poza plansze
		|| z + MISSILE_LENGTH / 2 > BOARD_SIZE / 2 || z - MISSILE_LENGTH / 2 < -BOARD_SIZE / 2)
	{
		for (unsigned int j = 0; j < missiles.size(); j++)	// szukamy pocisku ktory powinien zniknac
		{
			if (missiles[j].getX() == this->x && missiles[j].getZ() == this->z)
			{
				delete s_MissileVertices;
				delete s_MissileNormals;
				delete s_MissileIndices;
				missiles.erase(missiles.begin() + j);
				break;
			}
		}
		return;
	}
	for (unsigned int i = 0; i < obstacles.size(); i++)
	{
		if (ifCollise(x, z, MISSILE_LENGTH, MISSILE_WIDTH,
			obstacles[i].getX(), obstacles[i].getZ(), OBSTACLE_LENGTH, OBSTACLE_WIDTH))	// jesli nastapila kolizja z przeszkoda
		{	
			for (unsigned int j = 0; j < missiles.size(); j++)	// szukamy pocisku ktory powinien zniknac
			{
				if (missiles[j].getX() == this->x && missiles[j].getZ() == this->z)
				{
					delete s_MissileVertices;
					delete s_MissileNormals;
					delete s_MissileIndices;
					missiles.erase(missiles.begin() + j);
					break;
				}
			}
			obstacles[i].getHitted();	// uruchamiamy metode aby poinformowac przeszkode ze zostala trafiona
			return;
		}
	}
	for (unsigned int i = 0; i < tanks.size(); i++)
	{
		if (ifCollise(x, z, MISSILE_LENGTH, MISSILE_WIDTH,
			tanks[i].getX(), tanks[i].getZ(), TANK_BASE_LENGTH, TANK_BASE_WIDTH))	// jesli nastapila kolizja z czolgiem
		{
			for (unsigned int j = 0; j < missiles.size(); j++)	// szukamy pocisku ktory powinien zniknac
			{
				if (missiles[j].getX() == this->x && missiles[j].getZ() == this->z)
				{	
					delete s_MissileVertices;
					delete s_MissileNormals;
					delete s_MissileIndices;
					missiles.erase(missiles.begin() + j);
					break;
				}
			}
			tanks[i].getHitted();	// uruchamiamy metode aby poinformowac czolg ze zostal trafiony
			return;
		}
	}
	if (ifCollise(x, z, MISSILE_LENGTH, MISSILE_WIDTH,								// jesli nastapila kolizja z czolgiem gracza
		playerTank.getX(), playerTank.getZ(), TANK_BASE_LENGTH, TANK_BASE_WIDTH))
	{
		for (unsigned int j = 0; j < missiles.size(); j++)	// szukamy pocisku ktory powinien zniknac
		{
			if (missiles[j].getX() == this->x && missiles[j].getZ() == this->z)
			{
				delete s_MissileVertices;
				delete s_MissileNormals;
				delete s_MissileIndices;
				missiles.erase(missiles.begin() + j);
				break;
			}
		}
		playerTank.getHitted();	// uruchamiamy metode aby poinformowac czolg ze zostal trafiony
		return;
	}
}

float Missile::getX()
{
	return x;
}

float Missile::getZ()
{
	return z;
}

int Missile::getDirection()
{
	return direction;
}

void Missile::draw()
{

	GLfloat missile_diffuse[] = { 0.8, 0.0, 0.0, 1.0 };
	GLfloat missile_ambient[] = { 0.4, 0.0, 0.0, 1.0 };
	GLfloat missile_specular[] = { 0.0, 0.0, 0.0, 1.0 };

	glMaterialfv(GL_FRONT, GL_DIFFUSE, missile_diffuse);		// nalozenie tekstury
	glMaterialfv(GL_FRONT, GL_AMBIENT, missile_ambient);
	glMaterialfv(GL_FRONT, GL_SPECULAR, missile_specular);

	glPushMatrix();
		glTranslatef(x, TANK_BASE_HEIGHT + TANK_HEAD_HEIGHT / 2, z);
		glRotatef(direction * 90, 0, 1, 0);		// obrot w odpowiednim kierunku
		glScalef(6.0, 1.0, 1.0);	// "wydluzenie" pocisku
		glRotatef(90, 0, 0, 1);
		glVertexPointer(3, GL_FLOAT, 0, s_MissileVertices);
		glNormalPointer(GL_FLOAT, 0, s_MissileNormals);
		glDrawElements(GL_QUADS, s_MissileIndicesCount, GL_UNSIGNED_SHORT, s_MissileIndices);
	glPopMatrix();

}