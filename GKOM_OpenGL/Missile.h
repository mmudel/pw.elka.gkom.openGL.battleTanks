#ifndef MISSILE_H
#define MISSILE_H

#include <vector>
#include "Obstacle.h"
#include <iostream>
#include "TankConfiguration.h"

class Tank;
class PlayerTank;

extern std::vector<Obstacle> obstacles;
extern std::vector<Tank> tanks;
extern PlayerTank playerTank;

class Missile
{
private:
	float x, z;
	int direction;
	GLfloat* s_MissileVertices;
	GLfloat* s_MissileNormals;
	GLushort* s_MissileIndices;
	int s_MissileVerticesCount;
	int s_MissileIndicesCount;
public:
	Missile();
	Missile(float x, float z, int direction);
	~Missile();
	void move();
	float getX();
	float getZ();
	int getDirection();
	void draw();
};

#endif