/**

@author
	Michal Mudel

@thingsToDo
-Add multiplayer
-Add better camera
																*/
#include "PlayerTank.h"
#include "Tank.h"
#include "Missile.h"
#include "Obstacle.h"
#include "Camera.h"
#include "Background.h"
#include <vector>
#include <iterator>
#include "TankConfiguration.h"
#include <string>
#include <set>
#include <iostream>


using namespace std;

PlayerTank playerTank(0, 7, 0);	// czolg gracza
Background background;
Camera camera;				// kamera
vector<Missile> missiles;	// vector aktualnie lecacych pociskow
vector<Obstacle> obstacles;  // vector przeszkod na planszy
vector<Tank> tanks;			// vector czolgow na planszy
float lightness = 0.7;		// stopien swiecenia zrodla swiatla
int map[12][12] = {
	{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1 },
	{ 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1 },
	{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 },
	{ 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1 },
	{ 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1 },
	{ 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1 },
	{ 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1 },
	{ 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1 },
	{ 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1 },
	{ 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1 },
	{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 }
};

GLuint textures[TEXTURES_NO];

set<unsigned char> keySet;

void putObstacle(float x, float z)
{
	Obstacle *obstacle;
	obstacle = new Obstacle(x+0.75, z+0.75);
	obstacles.push_back(*obstacle);
	delete(obstacle);
}

void createObjects()
{
	Tank *tank;
	tank = new Tank(2, -2, 0);
	tanks.push_back(*tank);
	delete(tank);

	for (float x = 0; x < 12; x++)
	{
		for (float z = 0; z < 12; z++)
		{
			if (map[(int)z][(int)x] == 1)
				putObstacle(x*1.5 - 9, z*1.5 - 9);
		}
	}
}

GLuint load2DTexture(string filename)
{
	GLuint texture;
	glGenTextures(1, &texture);
	int width, height;
	string string_path = "textures\\" + filename;
	const char* path = string_path.c_str();

	glBindTexture(GL_TEXTURE_2D, texture);
	unsigned char* image =
		SOIL_load_image(path, &width, &height, 0, SOIL_LOAD_RGBA);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA,
		GL_UNSIGNED_BYTE, image);
	SOIL_free_image_data(image);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	return texture;
}

bool ifCollise(float x1, float z1, float lenght1, float width1, float x2, float z2, float lenght2, float width2)
{

	if (x1 + lenght1/2 <= x2 - lenght2/2) return false;
	if (x1 - lenght1/2 >= x2 + lenght2/2) return false;
	if (z1 + width1/2 <= z2 - width2/2) return false;
	if (z1 - width1/2 >= z2 + width2/2) return false;

	return true;
}

void keyPressed(unsigned char key, int mouseX, int mouseY)
{
	keySet.insert(key);
}

void keyReleased(unsigned char key, int mouseX, int mouseY)
{
	keySet.erase(key);
}

void keyAction()
{
	for (unsigned char key : keySet)
	{
		switch (key)
		{
		case 'w':
			playerTank.move(NORTH);
			break;
		case 'a':
			playerTank.move(WEST);
			break;
		case 's':
			playerTank.move(SOUTH);
			break;
		case 'd':
			playerTank.move(EAST);
			break;
		case 'j':
			camera.move(WEST);
			break;
		case 'i':
			camera.move(NORTH);
			break;
		case 'l':
			camera.move(EAST);
			break;
		case 'k':
			camera.move(SOUTH);
			break;
		case 'p':
			camera.reset();
			break;
		case 'y':
			if (lightness < 1.0)	lightness += 0.001;
			break;
		case 'h':
			if(lightness > 0.0)		lightness -= 0.001;
			break;
		case 32:			// 32 - spacja
			playerTank.fire();
			break;
		case 27:			// 27 - escape
			delete s_FloorNormals;
			exit(1);
		default:
			break;
		}
	}
}

void BruteForceNormalCalculation(GLfloat *v, int vcount, GLushort *ix, int count, GLfloat *n)
{
	//zero
	for (int i = 0; i < vcount * 3; i++)
	{
		n[i] = 0.0f;
	}
	//for each quad
	for (int i = 0; i < count; i += 4)
	{
		float nx, ny, nz;
		//calculate normal
		//we cheat! let's close our eyes and imagine, that our quad is really a triangle

		//variable names compatible with Wikipedia entry on cross product:)

		float a1 = v[ix[i + 1] * 3 + 0] - v[ix[i] * 3 + 0];
		float a2 = v[ix[i + 1] * 3 + 1] - v[ix[i] * 3 + 1];
		float a3 = v[ix[i + 1] * 3 + 2] - v[ix[i] * 3 + 2];

		float b1 = v[ix[i + 2] * 3 + 0] - v[ix[i] * 3 + 0];
		float b2 = v[ix[i + 2] * 3 + 1] - v[ix[i] * 3 + 1];
		float b3 = v[ix[i + 2] * 3 + 2] - v[ix[i] * 3 + 2];

		//cross product

		nx = a2 * b3 - a3 * b2;
		ny = a3 * b1 - a1 * b3;
		nz = a1 * b2 - a2 * b1;


		//normalize it, so that each quad contributes equally...
		float il = 1.0f / sqrt(nx * nx + ny * ny + nz * nz);
		nx *= il;
		ny *= il;
		nz *= il;

		//accumulate
		n[ix[i] * 3 + 0] += nx;
		n[ix[i] * 3 + 1] += ny;
		n[ix[i] * 3 + 2] += nz;

		n[ix[i + 1] * 3 + 0] += nx;
		n[ix[i + 1] * 3 + 1] += ny;
		n[ix[i + 1] * 3 + 2] += nz;

		n[ix[i + 2] * 3 + 0] += nx;
		n[ix[i + 2] * 3 + 1] += ny;
		n[ix[i + 2] * 3 + 2] += nz;

		n[ix[i + 3] * 3 + 0] += nx;
		n[ix[i + 3] * 3 + 1] += ny;
		n[ix[i + 3] * 3 + 2] += nz;
	}
}

void initializeObjectsData()
{
	s_FloorNormals = new GLfloat[s_FloorVerticesCount * 3];
	BruteForceNormalCalculation(s_FloorVertices, s_FloorVerticesCount, s_FloorIndices, s_FloorIndicesCount, s_FloorNormals);
}

void drawSea()
{
	static int water_number = 11;
	static int slow = 0;
	static float texture_offset = 0.0;
	//float offset_dif = 0.0001;
	GLfloat water_specular[] = { 0.0, 0.0, 0.0, 1.0 };
	GLfloat water_ambient[] = { 0.5, 0.5, 0.5, 1.0 };
	GLfloat water_diffuse[] = { 0.3, 0.3, 0.3, 1.0 };
	//texture_offset += offset_dif;
	//if (texture_offset >= 1.0f) texture_offset = 0.0f;
	texture_offset = 0.70;
	if (++slow >= 40)
	{
		slow = 0;
		if (++water_number > 57)
			water_number = 11;
	}
	glPushMatrix();

		glMaterialfv(GL_FRONT, GL_SPECULAR, water_specular);
		glMaterialfv(GL_FRONT, GL_AMBIENT, water_ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, water_diffuse);
		//glMaterialf(GL_FRONT, GL_SHININESS, 50.0);

		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, textures[water_number]);

		glBegin(GL_QUADS);
			glNormal3d(0, 1, 0);
			glTexCoord2f(0.0, 0.0);
			glVertex3f(-150, -0.1, 150);
			glTexCoord2f(1.0, 0.0);
			glVertex3f(150, -0.1, 150);
			glTexCoord2f(1.0, 0.7);
			glVertex3f(150, -0.1, -150);
			glTexCoord2f(0.0, 0.7);
			glVertex3f(-150, -0.1, -150);
		glEnd();

		glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}

void init()
{
	glShadeModel(GL_SMOOTH);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_NORMALIZE);

	glDepthFunc(GL_LESS);
	glEnable(GL_DEPTH_TEST);

	createObjects();

	textures[0] = load2DTexture("dirt.bmp");
	textures[1] = load2DTexture("obstacle.bmp");
	textures[2] = load2DTexture("cannon.bmp");
	textures[3] = load2DTexture("tankheadtop.bmp");
	textures[4] = load2DTexture("tankbasetop.bmp");
	textures[5] = load2DTexture("tankbaseside.bmp");
	textures[6] = load2DTexture("tankbasefront.bmp");
	textures[7] = load2DTexture("cannonfront.bmp");
	textures[8] = load2DTexture("tankbaseside2.bmp");
	textures[9] = load2DTexture("obstacledamaged.bmp");
	textures[10] = load2DTexture("background.bmp");
	string path;
	for (int i = 102; i <= 148; i++)				// tekstura wody od 11 do 57
	{
		path = "water";
		path += std::to_string(i);
		path += ".bmp";
		textures[i - 91] = load2DTexture(path);
	}
	textures[58] = load2DTexture("track1.bmp");
	textures[59] = load2DTexture("track2.bmp");
	textures[60] = load2DTexture("track3.bmp");
	textures[61] = load2DTexture("water3.bmp");
}



void displayObjects()
{
	GLfloat floor_diffuse[] = { 0.5, 0.5, 0.5, 1.0 };
	GLfloat floor_ambient[] = { 0.3, 0.3, 0.3, 1.0 };
	GLfloat floor_specular[] = { 0.0, 0.0, 0.0, 1.0 };

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY_EXT);
	
	glPushMatrix();
			
			/* Tworzenie czolgu gracza */

		playerTank.draw();

			/* Tworzenie pociskow */

		for (unsigned int i = 0; i < missiles.size(); i++)
			missiles[i].draw();

			/* Tworzenie przeszkod */

		for (unsigned int i = 0; i < obstacles.size(); i++)
			obstacles[i].draw();

			/* Tworzenie planszy */

		drawSea();		// malowanie morza (animacja)
		glPushMatrix();

			glMaterialfv(GL_FRONT, GL_DIFFUSE, floor_diffuse);		// nalozenie koloru
			glMaterialfv(GL_FRONT, GL_AMBIENT, floor_ambient);
			glMaterialfv(GL_FRONT, GL_SPECULAR, floor_specular);

			glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D, textures[0]);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glVertexPointer(3, GL_FLOAT, 0, s_FloorVertices);
			glTexCoordPointer(2, GL_FLOAT, 0, floorTexVertices);
			glNormalPointer(GL_FLOAT, 0, s_FloorNormals);
			glDrawElements(GL_QUADS, s_FloorIndicesCount, GL_UNSIGNED_SHORT, s_FloorIndices);
			glDisable(GL_TEXTURE_2D);
		glPopMatrix();

	glPopMatrix();

		/* Tworzenie pozostalych czolgow */

		for (unsigned int i = 0; i < tanks.size(); i++)
			tanks[i].draw();

		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_NORMAL_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY_EXT);
}

void motion()
{
	for (unsigned int i = 0; i < missiles.size(); i++)	// przesuniecie kazdego z pociskow
		missiles[i].move();
	for (unsigned int i = 0; i < tanks.size(); i++) 	// przesuniecie kazdego z czolgow
		tanks[i].move();
	playerTank.incReload();
}

void light()
{
	GLfloat light_position[] = { -1.0, 7.0, -20.0, 1.0 };
	GLfloat lm_ambient[] = { lightness, lightness, lightness, 1.0 };

	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lm_ambient);

}
void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	motion();
	keyAction();

	glPushMatrix();

	glPushMatrix();
		glTranslatef(0, 0, -30);
		glRotatef(270, 1, 0, 0);
		background.draw();
	glPopMatrix();

		camera.createLook();

		glPushMatrix();
			glScalef(1, 1, 1);
			displayObjects();
		glPopMatrix();

		light();

	glPopMatrix();
	glFlush();
	glutSwapBuffers();
}

void reshape(GLsizei w, GLsizei h)
{
	if (h > 0 && w > 0) {
		glViewport(0, 0, w, h);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		if (w <= h) {
			glFrustum(-1.5, 1.5, -1.5*h / w, 1.5*h / w, 1.0, 200.0);
		}
		else {
			glFrustum(-1.5*w / h, 1.5*w / h, -1.5, 1.5, 1.0, 200.0);
		}
		glMatrixMode(GL_MODELVIEW);
	}
}

int main(int argc, char** argv)
{
	initializeObjectsData();

	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);

	glutInitWindowPosition(0, 0);
	glutInitWindowSize(1024, 768);

	glutCreateWindow("Battle Tanks");

	glutKeyboardFunc(keyPressed);
	glutKeyboardUpFunc(keyReleased);
	glutFullScreen();

	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutIdleFunc(display);

	init();

	glutMainLoop();

	return 0;
}