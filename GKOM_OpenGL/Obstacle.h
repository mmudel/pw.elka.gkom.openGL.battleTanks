#ifndef OBSTACLE_H
#define OBSTACLE_H

#include <vector>
#include <iostream>
#include "TankConfiguration.h"

extern GLuint textures[TEXTURES_NO];

class Obstacle
{
private:
	float x, z;
	int hp;
public:
	Obstacle();
	Obstacle(float x, float z);
	~Obstacle();
	float getX();
	float getZ();
	int getHp();
	void getHitted();
	void draw();
};


#endif
