#ifndef PLAYERTANK_H
#define PLAYERTANK_H

#include "Tank.h"
#include "Obstacle.h"
#include <vector>

extern std::vector<Obstacle> obstacles;

class PlayerTank :
	public Tank
{
private:
	float cursorPosition;
public:
	PlayerTank(float x, float z, int direction);
	~PlayerTank();
	void move(int newDirection);
	void incReload();	// funkcja dodana w celu przeladowywania broni
	void getHitted();
	void draw();
};

#endif