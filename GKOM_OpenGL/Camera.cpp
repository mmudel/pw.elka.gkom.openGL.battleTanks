#include "Camera.h"
#include "TankConfiguration.h"


const float Camera::MIN_ANGLE_Y = 1.7;
const float Camera::MAX_ANGLE_Y = 2.7;
const float Camera::DEF_ANGLE_Y = 2.5;
const float Camera::DIF_ANGLE_Y = 0.001;

const float Camera::DEF_RADIUS = 11;

const float Camera::DEF_ANGLE_X = 0;
const float Camera::DIF_ANGLE_X = 0.001;

void Camera::createLook()
{
	gluLookAt(
		//eye position
		radius * cos(angleY) * sin(angleX),
		radius * sin(angleY),
		-radius * cos(angleY) * cos(angleX),
		//what am i looking at
		0,
		0,
		0,
		//up vector
		0, 1, 0);
}

Camera::Camera()
{
	angleX = DEF_ANGLE_X;
	angleY = DEF_ANGLE_Y;
	radius = DEF_RADIUS;
	createLook();
}


Camera::~Camera()
{
}

void Camera::move(int direction)
{
	switch (direction)
	{
	case NORTH:
		if (angleY > MIN_ANGLE_Y)
			angleY -= DIF_ANGLE_Y;
		break;
	case WEST:
		angleX -= DIF_ANGLE_X;
		break;
	case SOUTH:
		if (angleY < MAX_ANGLE_Y)
			angleY += DIF_ANGLE_Y;
		break;
	case EAST:
		angleX += DIF_ANGLE_X;
		break;
	}
}

void Camera::reset()
{
	angleX = DEF_ANGLE_X;
	angleY = DEF_ANGLE_Y;
	radius = DEF_RADIUS;
}