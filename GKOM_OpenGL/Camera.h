#ifndef CAMERA_H
#define CAMERA_H
#include "PlayerTank.h"

extern PlayerTank playerTank;

class Camera
{
private:
	float angleX;
	float angleY;
	float radius;

	static const float MIN_ANGLE_Y;
	static const float MAX_ANGLE_Y;
	static const float DEF_ANGLE_Y;
	static const float DIF_ANGLE_Y;

	static const float DEF_RADIUS;

	static const float DEF_ANGLE_X;
	static const float DIF_ANGLE_X;

public:
	Camera();
	~Camera();
	void move(int);
	void createLook();
	void reset();
};

#endif