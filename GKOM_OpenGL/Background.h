#ifndef BACKGROUND_H
#define BACKGROUND_H

#include "TankConfiguration.h"

extern GLuint textures[TEXTURES_NO];

class Background
{
private:
	float width;
	float height;
public:
	Background();
	~Background();
	void draw();
};

#endif