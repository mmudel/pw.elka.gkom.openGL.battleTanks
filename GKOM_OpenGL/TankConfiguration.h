#ifndef TANKCONFIGURATION_H
#define TANKCONFIGURATION_H

#include <Windows.h>
#include <GL/gl.h>
#include <glut.h>
#include <SOIL.h>

#define EAST 0
#define NORTH 1
#define WEST 2
#define SOUTH 3

#define TANK_BASE_LENGTH 1.5
#define TANK_BASE_WIDTH 1.2
#define TANK_BASE_HEIGHT 0.375

#define TANK_HEAD_LENGTH 0.825
#define TANK_HEAD_WIDTH 0.6
#define TANK_HEAD_HEIGHT 0.375

#define TANK_CANNON_LENGTH 0.75
#define TANK_CANNON_WIDTH 0.15
#define TANK_CANNON_HEIGHT 0.15

#define MISSILE_LENGTH 0.2
#define MISSILE_WIDTH 0.08
#define MISSILE_HEIGHT 0.08

#define OBSTACLE_LENGTH 1.5
#define OBSTACLE_WIDTH 1.5
#define OBSTACLE_HEIGHT 1.0

#define BOARD_SIZE 18


#define MISSILE_SPEED 0.02f
#define TANK_SPEED	0.004f

#define RELOAD_TIME 1000	// dlugosc przeladowywania dziala

#define MOVEMENT_LONG 50	// co ile czasu czolg NPC bedzie zmienial kierunek
#define RELOADING 1	// szybkosc przeladowywania dla czolgu NPC

#define BACKGROUND_WIDTH 160
#define BACKGROUND_HEIGHT 100

#define DEF_CURSOR_POSITION 0.4f		// polozenie maksymalne trojkata nad czolgiem
#define DIF_CURSOR_POSITION 0.001f		// wielkosc zmiany polozenia

#define TEXTURES_NO 62		// ilosc tekstur znajdujacych sie w programie

bool ifCollise(float, float, float, float, float, float, float, float);
void BruteForceNormalCalculation(GLfloat *v, int vcount, GLushort *ix, int count, GLfloat *n);

/*	Tworzenie siatek dla poszczegolnych obiektow	*/

/*	Podloze	*/

static GLfloat s_FloorVertices[] = {
	-BOARD_SIZE/2, 0.0,  BOARD_SIZE/2,
	 BOARD_SIZE/2, 0.0,  BOARD_SIZE/2,
	 BOARD_SIZE/2, 0.0, -BOARD_SIZE/2,
	-BOARD_SIZE/2, 0.0, -BOARD_SIZE/2,
};
static GLfloat floorTexVertices[] = { 0, 4, 0, 0, 4, 0, 4, 4 };
static GLfloat* s_FloorNormals = NULL;
static GLushort s_FloorIndices[] = {
	0, 1, 2, 3,
};
static int s_FloorVerticesCount = 4;	// 4 wierzcholki podloza
static int s_FloorIndicesCount = 1 * 4;	// 1 sciana, 4 wierzcholki


#endif