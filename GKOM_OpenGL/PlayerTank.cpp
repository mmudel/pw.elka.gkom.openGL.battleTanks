#include "PlayerTank.h"


PlayerTank::PlayerTank(float x, float z, int direction)
{
	this->x = x;
	this->z = z;
	this->direction = direction;
	hp = 3;
	cursorPosition = DEF_CURSOR_POSITION;
	trackNo = 58;
}


PlayerTank::~PlayerTank()
{
}

void PlayerTank::move(int newDirection)
{
	float moveX = 0, moveZ = 0;
	switch (direction)
	{
	case NORTH:
		moveX = 0.0f;
		moveZ = -TANK_SPEED;
		break;
	case WEST:
		moveX = -TANK_SPEED;
		moveZ = 0.0f;
		break;
	case SOUTH:
		moveX = 0.0f;
		moveZ = TANK_SPEED;
		break;
	case EAST:
		moveX = TANK_SPEED;
		moveZ = 0.0f;
		break;
	}
	if (newDirection != direction)
	{
		if (newDirection != (direction + 2) % 4)
		{
			for (unsigned int i = 0; i < obstacles.size(); i++)	// sprawdzamy czy nie wchodzimy na przeszkode
			{
				if (newDirection == EAST || newDirection == WEST)
				{
					if (ifCollise(x + moveX, z + moveZ, TANK_BASE_LENGTH, TANK_BASE_WIDTH,
						obstacles[i].getX(), obstacles[i].getZ(), OBSTACLE_LENGTH, OBSTACLE_WIDTH))
						return;
				}
				else
				{
					if (ifCollise(x + moveX, z + moveZ, TANK_BASE_WIDTH, TANK_BASE_LENGTH,
						obstacles[i].getX(), obstacles[i].getZ(), OBSTACLE_LENGTH, OBSTACLE_WIDTH))
						return;
				}
			}
		}
		direction = newDirection;
		return;
	}
	else
	{
		for (unsigned int i = 0; i < obstacles.size(); i++)
		{
			if (direction == EAST || direction == WEST)
			{
				if (ifCollise(x + moveX, z + moveZ, TANK_BASE_LENGTH, TANK_BASE_WIDTH,
					obstacles[i].getX(), obstacles[i].getZ(), OBSTACLE_LENGTH, OBSTACLE_WIDTH))
					return;
			}
			else
			{
				if (ifCollise(x + moveX, z + moveZ, TANK_BASE_WIDTH, TANK_BASE_LENGTH,
					obstacles[i].getX(), obstacles[i].getZ(), OBSTACLE_LENGTH, OBSTACLE_WIDTH))
					return;
			}
		}
		for (unsigned int i = 0; i < tanks.size(); i++)
		{
			if (ifCollise(x + moveX, z + moveZ, TANK_BASE_LENGTH, TANK_BASE_WIDTH,
				tanks[i].getX(), tanks[i].getZ(), TANK_BASE_LENGTH, TANK_BASE_WIDTH))
				return;
		}
		if (x + moveX + TANK_BASE_LENGTH / 2 > BOARD_SIZE / 2 || x + moveX - TANK_BASE_LENGTH / 2 < -BOARD_SIZE / 2
			|| z + moveZ + TANK_BASE_LENGTH / 2 > BOARD_SIZE / 2 || z + moveZ - TANK_BASE_LENGTH / 2 < -BOARD_SIZE / 2)
			return;
		trackNo++;
		if (trackNo > 60) trackNo = 58;
		x = x + moveX;
		z = z + moveZ;
	}
}

void PlayerTank::incReload()
{
	reloading++;
}

void PlayerTank::getHitted()
{
	hp--;
	if (hp == 0)
	{
		//@TODO
		std::cout << "Przegrales!!!" << std::endl;
	}
}

void PlayerTank::draw()
{
	GLfloat tank_diffuse[] = { 0.6, 0.6, 0.6, 1.0 };
	GLfloat tank_ambient[] = { 0.4, 0.4, 0.4, 1.0 };
	GLfloat tank_specular[] = { 0.0, 0.0, 0.0, 1.0 };
	GLfloat cursor_diffuse[] = { 0.0, 0.0, 1.0, 1.0 };
	GLfloat cursor_ambient[] = { 0.0, 0.0, 1.0, 1.0 };
	static float cursorPositionDif = DIF_CURSOR_POSITION;


	if (cursorPosition <= 0.0 || cursorPosition >= DEF_CURSOR_POSITION)	cursorPositionDif = -cursorPositionDif;
	cursorPosition += cursorPositionDif;

	glEnable(GL_TEXTURE_2D);

	glPushMatrix();
		glTranslatef(x, 0.0, z);	// ruch czolgu

		glMaterialfv(GL_FRONT, GL_DIFFUSE, cursor_diffuse);		// nalozenie tekstury
		glMaterialfv(GL_FRONT, GL_AMBIENT, cursor_ambient);

		/* Latajacy wskaznik */
		glPushMatrix();
			glBegin(GL_TRIANGLES);
			glVertex3f(-0.25, TANK_BASE_HEIGHT + TANK_HEAD_HEIGHT + 0.8+cursorPosition, 0.0);
			glVertex3f(0.25, TANK_BASE_HEIGHT + TANK_HEAD_HEIGHT + 0.8+cursorPosition, 0.0);
			glVertex3f(0.0, TANK_BASE_HEIGHT + TANK_HEAD_HEIGHT + 0.3+cursorPosition, 0.0);
			glEnd();
		glPopMatrix();

		glRotatef(direction * 90, 0, 1, 0);		// obrot w odpowiednim kierunku

		glMaterialfv(GL_FRONT, GL_DIFFUSE, tank_diffuse);		// nalozenie tekstury
		glMaterialfv(GL_FRONT, GL_AMBIENT, tank_ambient);
		glMaterialfv(GL_FRONT, GL_SPECULAR, tank_specular);

		glPushMatrix();	// podstawa

			glBindTexture(GL_TEXTURE_2D, textures[trackNo]);

			glBegin(GL_QUADS);
				glNormal3d(0, 0, 1);
				glTexCoord2f(0.0, 1.0);
				glVertex3f(-TANK_BASE_LENGTH / 2, 0.0, TANK_BASE_WIDTH / 2);
				glTexCoord2f(1.0, 1.0);
				glVertex3f(TANK_BASE_LENGTH / 2, 0.0, TANK_BASE_WIDTH / 2);
				glTexCoord2f(1.0, 0.0);
				glVertex3f(TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, TANK_BASE_WIDTH / 2);
				glTexCoord2f(0.0, 0.0);
				glVertex3f(-TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, TANK_BASE_WIDTH / 2);
			glEnd();

			glBindTexture(GL_TEXTURE_2D, textures[6]);

			glBegin(GL_QUADS);
				glNormal3d(1, 0, 0);
				glTexCoord2f(0.0, 1.0);
				glVertex3f(TANK_BASE_LENGTH / 2, 0.0, TANK_BASE_WIDTH / 2);
				glTexCoord2f(0.0, 0.0);
				glVertex3f(TANK_BASE_LENGTH / 2, 0.0, -TANK_BASE_WIDTH / 2);
				glTexCoord2f(1.0, 0.0);
				glVertex3f(TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, -TANK_BASE_WIDTH / 2);
				glTexCoord2f(1.0, 1.0);
				glVertex3f(TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, TANK_BASE_WIDTH / 2);
			glEnd();

			glBindTexture(GL_TEXTURE_2D, textures[trackNo]);

			glBegin(GL_QUADS);
				glNormal3d(0, 0, -1);
				glTexCoord2f(0.0, 1.0);
				glVertex3f(-TANK_BASE_LENGTH / 2, 0.0, -TANK_BASE_WIDTH / 2);
				glTexCoord2f(1.0, 1.0);
				glVertex3f(TANK_BASE_LENGTH / 2, 0.0, -TANK_BASE_WIDTH / 2);
				glTexCoord2f(1.0, 0.0);
				glVertex3f(TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, -TANK_BASE_WIDTH / 2);
				glTexCoord2f(0.0, 0.0);
				glVertex3f(-TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, -TANK_BASE_WIDTH / 2);
			glEnd();

			glBindTexture(GL_TEXTURE_2D, textures[6]);

			glBegin(GL_QUADS);
				glNormal3d(-1, 0, 0);
				glTexCoord2f(0.0, 1.0);
				glVertex3f(-TANK_BASE_LENGTH / 2, 0.0, -TANK_BASE_WIDTH / 2);
				glTexCoord2f(0.0, 0.0);
				glVertex3f(-TANK_BASE_LENGTH / 2, 0.0, TANK_BASE_WIDTH / 2);
				glTexCoord2f(1.0, 0.0);
				glVertex3f(-TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, TANK_BASE_WIDTH / 2);
				glTexCoord2f(1.0, 1.0);
				glVertex3f(-TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, -TANK_BASE_WIDTH / 2);
			glEnd();

			glBindTexture(GL_TEXTURE_2D, textures[4]);

			glBegin(GL_QUADS);
				glNormal3d(0, 1, 0);
				glTexCoord2f(0.0, 1.0);
				glVertex3f(-TANK_BASE_LENGTH / 2, 0.0, TANK_BASE_WIDTH / 2);
				glTexCoord2f(0.0, 0.0);
				glVertex3f(TANK_BASE_LENGTH / 2, 0.0, TANK_BASE_WIDTH / 2);
				glTexCoord2f(1.0, 0.0);
				glVertex3f(TANK_BASE_LENGTH / 2, 0.0, -TANK_BASE_WIDTH / 2);
				glTexCoord2f(1.0, 1.0);
				glVertex3f(-TANK_BASE_LENGTH / 2, 0.0, -TANK_BASE_WIDTH / 2);
			glEnd();

			glBegin(GL_QUADS);
				glNormal3d(0, -1, 0);
				glTexCoord2f(0.0, 1.0);
				glVertex3f(-TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, TANK_BASE_WIDTH / 2);
				glTexCoord2f(0.0, 0.0);
				glVertex3f(TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, TANK_BASE_WIDTH / 2);
				glTexCoord2f(1.0, 0.0);
				glVertex3f(TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, -TANK_BASE_WIDTH / 2);
				glTexCoord2f(1.0, 1.0);
				glVertex3f(-TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, -TANK_BASE_WIDTH / 2);
			glEnd();

			/*glVertexPointer(3, GL_FLOAT, 0, s_BaseVertices);
			glNormalPointer(GL_FLOAT, 0, s_BaseNormals);
			glDrawElements(GL_QUADS, s_BaseIndicesCount, GL_UNSIGNED_SHORT, s_BaseIndices);*/

		glPopMatrix();

		glPushMatrix();	// glowa
			glTranslatef(0.0, TANK_BASE_HEIGHT, 0.0);

			glBindTexture(GL_TEXTURE_2D, textures[6]);

			glBegin(GL_QUADS);
				glNormal3d(0, 0, 1);
				glTexCoord2f(0.0, 0.0);
				glVertex3f(-TANK_HEAD_LENGTH / 2, 0.0, TANK_HEAD_WIDTH / 2);
				glTexCoord2f(1.0, 0.0);
				glVertex3f(TANK_HEAD_LENGTH / 2, 0.0, TANK_HEAD_WIDTH / 2);
				glTexCoord2f(1.0, 1.0);
				glVertex3f(TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, TANK_HEAD_WIDTH / 2);
				glTexCoord2f(0.0, 1.0);
				glVertex3f(-TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, TANK_HEAD_WIDTH / 2);
			glEnd();

			glBegin(GL_QUADS);
				glNormal3d(1, 0, 0);
				glTexCoord2f(0.0, 1.0);
				glVertex3f(TANK_HEAD_LENGTH / 2, 0.0, TANK_HEAD_WIDTH / 2);
				glTexCoord2f(0.0, 0.0);
				glVertex3f(TANK_HEAD_LENGTH / 2, 0.0, -TANK_HEAD_WIDTH / 2);
				glTexCoord2f(1.0, 0.0);
				glVertex3f(TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, -TANK_HEAD_WIDTH / 2);
				glTexCoord2f(1.0, 1.0);
				glVertex3f(TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, TANK_HEAD_WIDTH / 2);
			glEnd();

			glBegin(GL_QUADS);
				glNormal3d(0, 0, -1);
				glTexCoord2f(0.0, 1.0);
				glVertex3f(TANK_HEAD_LENGTH / 2, 0.0, -TANK_HEAD_WIDTH / 2);
				glTexCoord2f(0.0, 0.0);
				glVertex3f(-TANK_HEAD_LENGTH / 2, 0.0, -TANK_HEAD_WIDTH / 2);
				glTexCoord2f(1.0, 0.0);
				glVertex3f(-TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, -TANK_HEAD_WIDTH / 2);
				glTexCoord2f(1.0, 1.0);
				glVertex3f(TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, -TANK_HEAD_WIDTH / 2);
			glEnd();

			glBegin(GL_QUADS);
				glNormal3d(-1, 0, 0);
				glTexCoord2f(0.0, 1.0);
				glVertex3f(-TANK_HEAD_LENGTH / 2, 0.0, -TANK_HEAD_WIDTH / 2);
				glTexCoord2f(0.0, 0.0);
				glVertex3f(-TANK_HEAD_LENGTH / 2, 0.0, TANK_HEAD_WIDTH / 2);
				glTexCoord2f(1.0, 0.0);
				glVertex3f(-TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, TANK_HEAD_WIDTH / 2);
				glTexCoord2f(1.0, 1.0);
				glVertex3f(-TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, -TANK_HEAD_WIDTH / 2);
			glEnd();

			glBegin(GL_QUADS);
				glNormal3d(0, 1, 0);
				glTexCoord2f(0.0, 1.0);
				glVertex3f(-TANK_HEAD_LENGTH / 2, 0.0, TANK_HEAD_WIDTH / 2);
				glTexCoord2f(0.0, 0.0);
				glVertex3f(TANK_HEAD_LENGTH / 2, 0.0, TANK_HEAD_WIDTH / 2);
				glTexCoord2f(1.0, 0.0);
				glVertex3f(TANK_HEAD_LENGTH / 2, 0.0, -TANK_HEAD_WIDTH / 2);
				glTexCoord2f(1.0, 1.0);
				glVertex3f(-TANK_HEAD_LENGTH / 2, 0.0, -TANK_HEAD_WIDTH / 2);
			glEnd();

			glBindTexture(GL_TEXTURE_2D, textures[3]);

			glBegin(GL_QUADS);
				glNormal3d(0, -1, 0);
				glTexCoord2f(0.0, 1.0);
				glVertex3f(-TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, TANK_HEAD_WIDTH / 2);
				glTexCoord2f(0.0, 0.0);
				glVertex3f(TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, TANK_HEAD_WIDTH / 2);
				glTexCoord2f(1.0, 0.0);
				glVertex3f(TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, -TANK_HEAD_WIDTH / 2);
				glTexCoord2f(1.0, 1.0);
				glVertex3f(-TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, -TANK_HEAD_WIDTH / 2);
			glEnd();

			/*glVertexPointer(3, GL_FLOAT, 0, s_HeadVertices);
			glNormalPointer(GL_FLOAT, 0, s_HeadNormals);
			glDrawElements(GL_QUADS, s_HeadIndicesCount, GL_UNSIGNED_SHORT, s_HeadIndices);*/
		glPopMatrix();

		glPushMatrix();	// lufa
			glTranslatef(TANK_HEAD_LENGTH / 2, TANK_BASE_HEIGHT + TANK_HEAD_HEIGHT / 2, 0.0);

			glBindTexture(GL_TEXTURE_2D, textures[2]);

			glBegin(GL_QUADS);
				glNormal3d(0, 0, 1);
				glTexCoord2f(0.0, 0.0);
				glVertex3f(-TANK_CANNON_LENGTH / 2, 0.0, TANK_CANNON_WIDTH / 2);
				glTexCoord2f(1.0, 0.0);
				glVertex3f(TANK_CANNON_LENGTH / 2, 0.0, TANK_CANNON_WIDTH / 2);
				glTexCoord2f(1.0, 1.0);
				glVertex3f(TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, TANK_CANNON_WIDTH / 2);
				glTexCoord2f(0.0, 1.0);
				glVertex3f(-TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, TANK_CANNON_WIDTH / 2);
			glEnd();

			glBindTexture(GL_TEXTURE_2D, textures[7]);

			glBegin(GL_QUADS);
				glNormal3d(1, 0, 0);
				glTexCoord2f(0.0, 1.0);
				glVertex3f(TANK_CANNON_LENGTH / 2, 0.0, TANK_CANNON_WIDTH / 2);
				glTexCoord2f(0.0, 0.0);
				glVertex3f(TANK_CANNON_LENGTH / 2, 0.0, -TANK_CANNON_WIDTH / 2);
				glTexCoord2f(1.0, 0.0);
				glVertex3f(TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, -TANK_CANNON_WIDTH / 2);
				glTexCoord2f(1.0, 1.0);
				glVertex3f(TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, TANK_CANNON_WIDTH / 2);
			glEnd();

			glBindTexture(GL_TEXTURE_2D, textures[2]);

			glBegin(GL_QUADS);
				glNormal3d(0, 0, -1);
				glTexCoord2f(0.0, 1.0);
				glVertex3f(TANK_CANNON_LENGTH / 2, 0.0, -TANK_CANNON_WIDTH / 2);
				glTexCoord2f(0.0, 0.0);
				glVertex3f(-TANK_CANNON_LENGTH / 2, 0.0, -TANK_CANNON_WIDTH / 2);
				glTexCoord2f(1.0, 0.0);
				glVertex3f(-TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, -TANK_CANNON_WIDTH / 2);
				glTexCoord2f(1.0, 1.0);
				glVertex3f(TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, -TANK_CANNON_WIDTH / 2);
			glEnd();

			glBegin(GL_QUADS);
				glNormal3d(-1, 0, 0);
				glTexCoord2f(0.0, 1.0);
				glVertex3f(-TANK_CANNON_LENGTH / 2, 0.0, -TANK_CANNON_WIDTH / 2);
				glTexCoord2f(0.0, 0.0);
				glVertex3f(-TANK_CANNON_LENGTH / 2, 0.0, TANK_CANNON_WIDTH / 2);
				glTexCoord2f(1.0, 0.0);
				glVertex3f(-TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, TANK_CANNON_WIDTH / 2);
				glTexCoord2f(1.0, 1.0);
				glVertex3f(-TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, -TANK_CANNON_WIDTH / 2);
			glEnd();

			glBegin(GL_QUADS);
				glNormal3d(0, 1, 0);
				glTexCoord2f(0.0, 1.0);
				glVertex3f(-TANK_CANNON_LENGTH / 2, 0.0, TANK_CANNON_WIDTH / 2);
				glTexCoord2f(0.0, 0.0);
				glVertex3f(TANK_CANNON_LENGTH / 2, 0.0, TANK_CANNON_WIDTH / 2);
				glTexCoord2f(1.0, 0.0);
				glVertex3f(TANK_CANNON_LENGTH / 2, 0.0, -TANK_CANNON_WIDTH / 2);
				glTexCoord2f(1.0, 1.0);
				glVertex3f(-TANK_CANNON_LENGTH / 2, 0.0, -TANK_CANNON_WIDTH / 2);
			glEnd();

			glBegin(GL_QUADS);
				glNormal3d(0, -1, 0);
				glTexCoord2f(0.0, 1.0);
				glVertex3f(-TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, TANK_CANNON_WIDTH / 2);
				glTexCoord2f(0.0, 0.0);
				glVertex3f(TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, TANK_CANNON_WIDTH / 2);
				glTexCoord2f(1.0, 0.0);
				glVertex3f(TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, -TANK_CANNON_WIDTH / 2);
				glTexCoord2f(1.0, 1.0);
				glVertex3f(-TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, -TANK_CANNON_WIDTH / 2);
			glEnd();

			/*glVertexPointer(3, GL_FLOAT, 0, s_CannonVertices);
			glNormalPointer(GL_FLOAT, 0, s_CannonNormals);
			glDrawElements(GL_QUADS, s_CannonIndicesCount, GL_UNSIGNED_SHORT, s_CannonIndices);*/
		glPopMatrix();

	glPopMatrix();

	glDisable(GL_TEXTURE_2D);
}