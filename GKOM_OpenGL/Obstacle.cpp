#include "Obstacle.h"

extern std::vector<Obstacle> obstacles;

Obstacle::Obstacle()
{
}


Obstacle::~Obstacle()
{
}


Obstacle::Obstacle(float x, float z)
{
	this->x = x;
	this->z = z;
	this->hp = 2;
}

float Obstacle::getX()
{
	return x;
}

float Obstacle::getZ()
{
	return z;
}

int Obstacle::getHp()
{
	return hp;
}

void Obstacle::getHitted()
{
	hp--;
	if (hp == 0)
	{
		for (unsigned int i = 0; i < obstacles.size(); i++)	// szukamy przeszkody ktora powinna zniknac
		{
			if (obstacles[i].getHp() == 0)
			{
				obstacles.erase(obstacles.begin() + i);
				break;
			}
		}
	}
}

void Obstacle::draw()
{
	GLfloat obstacle_diffuse[] = { 0.7, 0.7, 0.7, 1.0 };
	GLfloat obstacle_ambient[] = { 0.3, 0.3, 0.3, 1.0 };
	GLfloat obstacle_specular[] = { 1.0, 1.0, 1.0, 1.0 };
	
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, obstacle_diffuse);		// nalozenie tekstury
	glMaterialfv(GL_FRONT, GL_AMBIENT, obstacle_ambient);
	glMaterialfv(GL_FRONT, GL_SPECULAR, obstacle_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, 50.0);

	glPushMatrix();
		glTranslatef(x, 0.0, z);
		//glMaterialf(GL_FRONT, GL_SHININESS, 1.0);

		glEnable(GL_TEXTURE_2D);

		if (hp==2)
			glBindTexture(GL_TEXTURE_2D, textures[1]);
		else
			glBindTexture(GL_TEXTURE_2D, textures[9]);

		glBegin(GL_QUADS);
			glNormal3d(0, 0, 1);
			glTexCoord2f(0.0, 1.0);
			glVertex3f(-OBSTACLE_LENGTH / 2, 0.0, OBSTACLE_WIDTH / 2);
			glTexCoord2f(0.0, 0.0);
			glVertex3f(OBSTACLE_LENGTH / 2, 0.0, OBSTACLE_WIDTH / 2);
			glTexCoord2f(1.0, 0.0);
			glVertex3f(OBSTACLE_LENGTH / 2, OBSTACLE_HEIGHT, OBSTACLE_WIDTH / 2);
			glTexCoord2f(1.0, 1.0);
			glVertex3f(-OBSTACLE_LENGTH / 2, OBSTACLE_HEIGHT, OBSTACLE_WIDTH / 2);
		glEnd();

		glBegin(GL_QUADS);
			glNormal3d(1, 0, 0);
			glTexCoord2f(0.0, 1.0);
			glVertex3f(OBSTACLE_LENGTH / 2, 0.0, OBSTACLE_WIDTH / 2);
			glTexCoord2f(0.0, 0.0);
			glVertex3f(OBSTACLE_LENGTH / 2, 0.0, -OBSTACLE_WIDTH / 2);
			glTexCoord2f(1.0, 0.0);
			glVertex3f(OBSTACLE_LENGTH / 2, OBSTACLE_HEIGHT, -OBSTACLE_WIDTH / 2);
			glTexCoord2f(1.0, 1.0);
			glVertex3f(OBSTACLE_LENGTH / 2, OBSTACLE_HEIGHT, OBSTACLE_WIDTH / 2);
		glEnd();

		glBegin(GL_QUADS);
			glNormal3d(0, 0, -1);
			glTexCoord2f(0.0, 1.0);
			glVertex3f(OBSTACLE_LENGTH / 2, 0.0, -OBSTACLE_WIDTH / 2);
			glTexCoord2f(0.0, 0.0);
			glVertex3f(-OBSTACLE_LENGTH / 2, 0.0, -OBSTACLE_WIDTH / 2);
			glTexCoord2f(1.0, 0.0);
			glVertex3f(-OBSTACLE_LENGTH / 2, OBSTACLE_HEIGHT, -OBSTACLE_WIDTH / 2);
			glTexCoord2f(1.0, 1.0);
			glVertex3f(OBSTACLE_LENGTH / 2, OBSTACLE_HEIGHT, -OBSTACLE_WIDTH / 2);
		glEnd();

		glBegin(GL_QUADS);
			glNormal3d(-1, 0, 0);
			glTexCoord2f(0.0, 1.0);
			glVertex3f(-OBSTACLE_LENGTH / 2, 0.0, -OBSTACLE_WIDTH / 2);
			glTexCoord2f(0.0, 0.0);
			glVertex3f(-OBSTACLE_LENGTH / 2, 0.0, OBSTACLE_WIDTH / 2);
			glTexCoord2f(1.0, 0.0);
			glVertex3f(-OBSTACLE_LENGTH / 2, OBSTACLE_HEIGHT, OBSTACLE_WIDTH / 2);
			glTexCoord2f(1.0, 1.0);
			glVertex3f(-OBSTACLE_LENGTH / 2, OBSTACLE_HEIGHT, -OBSTACLE_WIDTH / 2);
		glEnd();

		glBegin(GL_QUADS);
			glNormal3d(0, 1, 0);
			glTexCoord2f(0.0, 1.0);
			glVertex3f(-OBSTACLE_LENGTH / 2, 0.0, OBSTACLE_WIDTH / 2);
			glTexCoord2f(0.0, 0.0);
			glVertex3f(OBSTACLE_LENGTH / 2, 0.0, OBSTACLE_WIDTH / 2);
			glTexCoord2f(1.0, 0.0);
			glVertex3f(OBSTACLE_LENGTH / 2, 0.0, -OBSTACLE_WIDTH / 2);
			glTexCoord2f(1.0, 1.0);
			glVertex3f(-OBSTACLE_LENGTH / 2, 0.0, -OBSTACLE_WIDTH / 2);
		glEnd();

		glBegin(GL_QUADS);
			glNormal3d(0, -1, 0);
			glTexCoord2f(0.0, 1.0);
			glVertex3f(-OBSTACLE_LENGTH / 2, OBSTACLE_HEIGHT, OBSTACLE_WIDTH / 2);
			glTexCoord2f(0.0, 0.0);
			glVertex3f(OBSTACLE_LENGTH / 2, OBSTACLE_HEIGHT, OBSTACLE_WIDTH / 2);
			glTexCoord2f(1.0, 0.0);
			glVertex3f(OBSTACLE_LENGTH / 2, OBSTACLE_HEIGHT, -OBSTACLE_WIDTH / 2);
			glTexCoord2f(1.0, 1.0);
			glVertex3f(-OBSTACLE_LENGTH / 2, OBSTACLE_HEIGHT, -OBSTACLE_WIDTH / 2);
		glEnd();

		glDisable(GL_TEXTURE_2D);

	glPopMatrix();
}