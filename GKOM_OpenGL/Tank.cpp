#include "Tank.h"
#include "PlayerTank.h"
#include <time.h>


Tank::Tank(float x, float z, int direction)
{
	this->x = x;
	this->z = z;
	this->direction = direction;
	hp = 3;
	movementLong = 0;
	reloading = RELOAD_TIME;
	dead = false;
	trackNo = 58;
}

Tank::Tank()
{

}

Tank::~Tank()
{
}


void Tank::move()
{
	if (dead == true)
		return;
	srand(time(NULL));
	int newDirection = direction;
	float moveX, moveZ;
	movementLong++;
	reloading+=RELOADING;
	this->fire();
	if (movementLong >= MOVEMENT_LONG)
	{
		newDirection = ((rand() * rand() + rand()) % 4);
		movementLong = 0;
	}
	switch (newDirection)
	{
	case 0:
		moveX = TANK_SPEED;
		moveZ = 0.0f;
		break;
	case 1:
		moveX = 0.0f;
		moveZ = -TANK_SPEED;
		break;
	case 2:
		moveX = -TANK_SPEED;
		moveZ = 0.0f;
		break;
	case 3:
		moveX = 0.0f;
		moveZ = TANK_SPEED;
	}
	if (newDirection != direction)
	{
		for (unsigned int i = 0; i < obstacles.size(); i++)	// sprawdzamy czy nie wchodzimy na przeszkode
		{
			if (newDirection == EAST || newDirection == WEST)
			{
				if (ifCollise(x + moveX, z + moveZ, TANK_BASE_LENGTH, TANK_BASE_WIDTH,
					obstacles[i].getX(), obstacles[i].getZ(), OBSTACLE_LENGTH, OBSTACLE_WIDTH))
					return;
			}
			else
			{
				if (ifCollise(x + moveX, z + moveZ, TANK_BASE_WIDTH, TANK_BASE_LENGTH,
					obstacles[i].getX(), obstacles[i].getZ(), OBSTACLE_LENGTH, OBSTACLE_WIDTH))
					return;
			}
		}
		direction = newDirection;
		return;
	}
	else
	{
		if (x + moveX + TANK_BASE_LENGTH / 2 > BOARD_SIZE / 2 || x + moveX - TANK_BASE_LENGTH / 2 < -BOARD_SIZE / 2			// sprawdzamy czy nie wychodzimy za plansze
			|| z + moveZ + TANK_BASE_LENGTH / 2 > BOARD_SIZE / 2 || z + moveZ - TANK_BASE_LENGTH / 2 < -BOARD_SIZE / 2)
		{
			movementLong = MOVEMENT_LONG;
			return;
		}
		for (unsigned int i = 0; i < obstacles.size(); i++)	// sprawdzamy czy nie wchodzimy na przeszkode
		{
			if (direction == EAST || direction == WEST)
			{
				if (ifCollise(x + moveX, z + moveZ, TANK_BASE_LENGTH, TANK_BASE_WIDTH,
					obstacles[i].getX(), obstacles[i].getZ(), OBSTACLE_LENGTH, OBSTACLE_WIDTH))
				{
					movementLong = MOVEMENT_LONG;
					return;
				}
			}
			else
			{
				if (ifCollise(x + moveX, z + moveZ, TANK_BASE_WIDTH, TANK_BASE_LENGTH,
					obstacles[i].getX(), obstacles[i].getZ(), OBSTACLE_LENGTH, OBSTACLE_WIDTH))
				{
					movementLong = MOVEMENT_LONG;
					return;
				}
			}
		}
		for (unsigned int i = 0; i < tanks.size(); i++)		// sprawdzamy czy nie wchodzimy na inny czolg
		{
			if (x == tanks[i].getX() && z == tanks[i].getZ())	// jesli sprawdzamy dla siebie samego
				continue;
			if (ifCollise(x + moveX, z + moveZ, TANK_BASE_LENGTH, TANK_BASE_WIDTH,
				tanks[i].getX(), tanks[i].getZ(), TANK_BASE_LENGTH, TANK_BASE_WIDTH))
			{
				movementLong = MOVEMENT_LONG;
				return;
			}
		}
		if (ifCollise(x + moveX, z + moveZ, TANK_BASE_LENGTH, TANK_BASE_WIDTH,			// sprawdzamy czy nie wchodzimy na czolg gracza
			playerTank.getX(), playerTank.getZ(), TANK_BASE_LENGTH, TANK_BASE_WIDTH))
		{
			movementLong = MOVEMENT_LONG;
			return;
		}
		trackNo++;
		if (trackNo > 60) trackNo = 58;
		x = x + moveX;
		z = z + moveZ;
	}
}

void Tank::fire()
{
	float missileX, missileZ, missileDirection;
	float shift; // przesuniecie w celu pojawienia sie pocisku przy koncu lufy
	Missile *newMissile;
	missileDirection = direction;
	shift = TANK_HEAD_LENGTH / 2 + TANK_CANNON_LENGTH;
	if (reloading < RELOAD_TIME)
		return;
	reloading = 0;
	switch (direction)
	{
	case 0:
		missileX = x + shift;
		missileZ = z;
		break;
	case 1:
		missileX = x;
		missileZ = z - shift;
		break;
	case 2:
		missileX = x - shift;
		missileZ = z;
		break;
	case 3:
		missileX = x;
		missileZ = z + shift;
		break;
	}
	newMissile = new Missile(missileX, missileZ, missileDirection);
	missiles.push_back(*newMissile);
	delete(newMissile);
}

float Tank::getX()
{
	return x;
}

float Tank::getZ()
{
	return z;
}

int Tank::getDirection()
{
	return direction;
}

void Tank::getHitted()
{
	hp--;
	if (hp == 0)
	{
		dead = true;
	}
}

int Tank::getHp()
{
	return hp;
}

void Tank::draw()
{
	GLfloat tank_diffuse[] = { 0.6, 0.6, 0.6, 1.0 };
	GLfloat tank_ambient[] = { 0.4, 0.4, 0.4, 1.0 };
	GLfloat tank_specular[] = { 0.0, 0.0, 0.0, 1.0 };

	glMaterialfv(GL_FRONT, GL_DIFFUSE, tank_diffuse);		// nalozenie tekstury
	glMaterialfv(GL_FRONT, GL_AMBIENT, tank_ambient);
	glMaterialfv(GL_FRONT, GL_SPECULAR, tank_specular);

	if (dead == false)
		drawAlive();
	else
		drawDead();

}

void Tank::drawAlive()
{
	glEnable(GL_TEXTURE_2D);

	glPushMatrix();
	glTranslatef(x, 0.0, z);	// ruch czolgu	
	glRotatef(direction * 90, 0, 1, 0);		// obrot w odpowiednim kierunku


	glPushMatrix();	// podstawa

	glBindTexture(GL_TEXTURE_2D, textures[trackNo]);

	glBegin(GL_QUADS);
	glNormal3d(0, 0, 1);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(-TANK_BASE_LENGTH / 2, 0.0, TANK_BASE_WIDTH / 2);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(TANK_BASE_LENGTH / 2, 0.0, TANK_BASE_WIDTH / 2);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, TANK_BASE_WIDTH / 2);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(-TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, TANK_BASE_WIDTH / 2);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, textures[6]);

	glBegin(GL_QUADS);
	glNormal3d(1, 0, 0);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(TANK_BASE_LENGTH / 2, 0.0, TANK_BASE_WIDTH / 2);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(TANK_BASE_LENGTH / 2, 0.0, -TANK_BASE_WIDTH / 2);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, -TANK_BASE_WIDTH / 2);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, TANK_BASE_WIDTH / 2);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, textures[trackNo]);

	glBegin(GL_QUADS);
	glNormal3d(0, 0, -1);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(-TANK_BASE_LENGTH / 2, 0.0, -TANK_BASE_WIDTH / 2);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(TANK_BASE_LENGTH / 2, 0.0, -TANK_BASE_WIDTH / 2);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, -TANK_BASE_WIDTH / 2);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(-TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, -TANK_BASE_WIDTH / 2);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, textures[6]);

	glBegin(GL_QUADS);
	glNormal3d(-1, 0, 0);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(-TANK_BASE_LENGTH / 2, 0.0, -TANK_BASE_WIDTH / 2);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(-TANK_BASE_LENGTH / 2, 0.0, TANK_BASE_WIDTH / 2);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(-TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, TANK_BASE_WIDTH / 2);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(-TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, -TANK_BASE_WIDTH / 2);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, textures[4]);

	glBegin(GL_QUADS);
	glNormal3d(0, 1, 0);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(-TANK_BASE_LENGTH / 2, 0.0, TANK_BASE_WIDTH / 2);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(TANK_BASE_LENGTH / 2, 0.0, TANK_BASE_WIDTH / 2);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(TANK_BASE_LENGTH / 2, 0.0, -TANK_BASE_WIDTH / 2);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(-TANK_BASE_LENGTH / 2, 0.0, -TANK_BASE_WIDTH / 2);
	glEnd();

	glBegin(GL_QUADS);
	glNormal3d(0, -1, 0);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(-TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, TANK_BASE_WIDTH / 2);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, TANK_BASE_WIDTH / 2);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, -TANK_BASE_WIDTH / 2);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(-TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, -TANK_BASE_WIDTH / 2);
	glEnd();

	glPopMatrix();

	glPushMatrix();	// glowa
	glTranslatef(0.0, TANK_BASE_HEIGHT, 0.0);

	glBindTexture(GL_TEXTURE_2D, textures[6]);

	glBegin(GL_QUADS);
	glNormal3d(0, 0, 1);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(-TANK_HEAD_LENGTH / 2, 0.0, TANK_HEAD_WIDTH / 2);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(TANK_HEAD_LENGTH / 2, 0.0, TANK_HEAD_WIDTH / 2);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, TANK_HEAD_WIDTH / 2);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(-TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, TANK_HEAD_WIDTH / 2);
	glEnd();

	glBegin(GL_QUADS);
	glNormal3d(1, 0, 0);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(TANK_HEAD_LENGTH / 2, 0.0, TANK_HEAD_WIDTH / 2);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(TANK_HEAD_LENGTH / 2, 0.0, -TANK_HEAD_WIDTH / 2);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, -TANK_HEAD_WIDTH / 2);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, TANK_HEAD_WIDTH / 2);
	glEnd();

	glBegin(GL_QUADS);
	glNormal3d(0, 0, -1);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(TANK_HEAD_LENGTH / 2, 0.0, -TANK_HEAD_WIDTH / 2);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(-TANK_HEAD_LENGTH / 2, 0.0, -TANK_HEAD_WIDTH / 2);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(-TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, -TANK_HEAD_WIDTH / 2);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, -TANK_HEAD_WIDTH / 2);
	glEnd();

	glBegin(GL_QUADS);
	glNormal3d(-1, 0, 0);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(-TANK_HEAD_LENGTH / 2, 0.0, -TANK_HEAD_WIDTH / 2);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(-TANK_HEAD_LENGTH / 2, 0.0, TANK_HEAD_WIDTH / 2);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(-TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, TANK_HEAD_WIDTH / 2);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(-TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, -TANK_HEAD_WIDTH / 2);
	glEnd();

	glBegin(GL_QUADS);
	glNormal3d(0, 1, 0);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(-TANK_HEAD_LENGTH / 2, 0.0, TANK_HEAD_WIDTH / 2);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(TANK_HEAD_LENGTH / 2, 0.0, TANK_HEAD_WIDTH / 2);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(TANK_HEAD_LENGTH / 2, 0.0, -TANK_HEAD_WIDTH / 2);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(-TANK_HEAD_LENGTH / 2, 0.0, -TANK_HEAD_WIDTH / 2);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, textures[3]);

	glBegin(GL_QUADS);
	glNormal3d(0, -1, 0);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(-TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, TANK_HEAD_WIDTH / 2);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, TANK_HEAD_WIDTH / 2);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, -TANK_HEAD_WIDTH / 2);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(-TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, -TANK_HEAD_WIDTH / 2);
	glEnd();

	glPopMatrix();

	glPushMatrix();	// lufa
	glTranslatef(TANK_HEAD_LENGTH / 2, TANK_BASE_HEIGHT + TANK_HEAD_HEIGHT / 2, 0.0);

	glBindTexture(GL_TEXTURE_2D, textures[2]);

	glBegin(GL_QUADS);
	glNormal3d(0, 0, 1);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(-TANK_CANNON_LENGTH / 2, 0.0, TANK_CANNON_WIDTH / 2);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(TANK_CANNON_LENGTH / 2, 0.0, TANK_CANNON_WIDTH / 2);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, TANK_CANNON_WIDTH / 2);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(-TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, TANK_CANNON_WIDTH / 2);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, textures[7]);

	glBegin(GL_QUADS);
	glNormal3d(1, 0, 0);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(TANK_CANNON_LENGTH / 2, 0.0, TANK_CANNON_WIDTH / 2);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(TANK_CANNON_LENGTH / 2, 0.0, -TANK_CANNON_WIDTH / 2);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, -TANK_CANNON_WIDTH / 2);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, TANK_CANNON_WIDTH / 2);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, textures[2]);

	glBegin(GL_QUADS);
	glNormal3d(0, 0, -1);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(TANK_CANNON_LENGTH / 2, 0.0, -TANK_CANNON_WIDTH / 2);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(-TANK_CANNON_LENGTH / 2, 0.0, -TANK_CANNON_WIDTH / 2);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(-TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, -TANK_CANNON_WIDTH / 2);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, -TANK_CANNON_WIDTH / 2);
	glEnd();

	glBegin(GL_QUADS);
	glNormal3d(-1, 0, 0);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(-TANK_CANNON_LENGTH / 2, 0.0, -TANK_CANNON_WIDTH / 2);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(-TANK_CANNON_LENGTH / 2, 0.0, TANK_CANNON_WIDTH / 2);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(-TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, TANK_CANNON_WIDTH / 2);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(-TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, -TANK_CANNON_WIDTH / 2);
	glEnd();

	glBegin(GL_QUADS);
	glNormal3d(0, 1, 0);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(-TANK_CANNON_LENGTH / 2, 0.0, TANK_CANNON_WIDTH / 2);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(TANK_CANNON_LENGTH / 2, 0.0, TANK_CANNON_WIDTH / 2);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(TANK_CANNON_LENGTH / 2, 0.0, -TANK_CANNON_WIDTH / 2);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(-TANK_CANNON_LENGTH / 2, 0.0, -TANK_CANNON_WIDTH / 2);
	glEnd();

	glBegin(GL_QUADS);
	glNormal3d(0, -1, 0);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(-TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, TANK_CANNON_WIDTH / 2);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, TANK_CANNON_WIDTH / 2);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, -TANK_CANNON_WIDTH / 2);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(-TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, -TANK_CANNON_WIDTH / 2);
	glEnd();

	glPopMatrix();

	glPopMatrix();

	glDisable(GL_TEXTURE_2D);
}

void Tank::drawDead()
{
	static float difX, difZ, difY = 0.005, difAngle = 0.45;
	static float newX=0, newZ=0, newY=0, newAngle=0;
	static float blending=1.0, difBlending = 0.001;
	int angleX=0, angleZ = 0;
	static bool stop = false;
	static bool reverse = false;
	switch (direction)
	{
	case NORTH:
		difX = 0.0;
		difZ = 0.003;
		angleX = 1;
		angleZ = 0;
		break;
	case WEST:
		difX = 0.003;
		difZ = 0.0;
		angleX = 0;
		angleZ = -1;
		break;
	case SOUTH:
		difX = 0.0;
		difZ = -0.003;
		angleX = -1;
		angleZ = 0;
		break;
	case EAST:
		difX = -0.003;
		difZ = 0.0;
		angleX = 0;
		angleZ = 1;
		break;
	}

	if (stop == false) { newX += difX; newZ += difZ; newY += difY; newAngle += difAngle; }
	if (newY >= 1)
	{
		difY = -difY;
		reverse = true;
	}
	if (reverse == true && newY <= 0.0)
		stop = true;
	if (stop == true)
	{
		GLfloat tank_diffuse_blending[] = { 0.6, 0.6, 0.6, blending};
		GLfloat tank_ambient_blending[] = { 0.4, 0.4, 0.4, blending};
		GLfloat tank_specular_blending[] = { 0.0, 0.0, 0.0, blending};

		glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, tank_diffuse_blending);		// nalozenie tekstury
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, tank_ambient_blending);
		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, tank_specular_blending);
		blending -= difBlending;
		if (blending <= 0.0)
			erase();
	}
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glPushMatrix();
		glTranslatef(x, 0.0, z);	// ruch czolgu	


		glPushMatrix();	// podstawa
			glRotatef(direction * 90, 0, 1, 0);		// obrot w odpowiednim kierunku
			glBindTexture(GL_TEXTURE_2D, textures[5]);

			glBegin(GL_QUADS);
				glNormal3d(0, 0, 1);
				glTexCoord2f(0.0, 0.0);
				glVertex3f(-TANK_BASE_LENGTH / 2, 0.0, TANK_BASE_WIDTH / 2);
				glTexCoord2f(1.0, 0.0);
				glVertex3f(TANK_BASE_LENGTH / 2, 0.0, TANK_BASE_WIDTH / 2);
				glTexCoord2f(1.0, 1.0);
				glVertex3f(TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, TANK_BASE_WIDTH / 2);
				glTexCoord2f(0.0, 1.0);
				glVertex3f(-TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, TANK_BASE_WIDTH / 2);
			glEnd();

			glBindTexture(GL_TEXTURE_2D, textures[6]);

			glBegin(GL_QUADS);
				glNormal3d(1, 0, 0);
				glTexCoord2f(0.0, 1.0);
				glVertex3f(TANK_BASE_LENGTH / 2, 0.0, TANK_BASE_WIDTH / 2);
				glTexCoord2f(0.0, 0.0);
				glVertex3f(TANK_BASE_LENGTH / 2, 0.0, -TANK_BASE_WIDTH / 2);
				glTexCoord2f(1.0, 0.0);
				glVertex3f(TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, -TANK_BASE_WIDTH / 2);
				glTexCoord2f(1.0, 1.0);
				glVertex3f(TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, TANK_BASE_WIDTH / 2);
			glEnd();

			glBindTexture(GL_TEXTURE_2D, textures[5]);

			glBegin(GL_QUADS);
				glNormal3d(0, 0, -1);
				glTexCoord2f(0.0, 0.0);
				glVertex3f(-TANK_BASE_LENGTH / 2, 0.0, -TANK_BASE_WIDTH / 2);
				glTexCoord2f(1.0, 0.0);
				glVertex3f(TANK_BASE_LENGTH / 2, 0.0, -TANK_BASE_WIDTH / 2);
				glTexCoord2f(1.0, 1.0);
				glVertex3f(TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, -TANK_BASE_WIDTH / 2);
				glTexCoord2f(0.0, 1.0);
				glVertex3f(-TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, -TANK_BASE_WIDTH / 2);
			glEnd();

			glBindTexture(GL_TEXTURE_2D, textures[6]);

			glBegin(GL_QUADS);
				glNormal3d(-1, 0, 0);
				glTexCoord2f(0.0, 1.0);
				glVertex3f(-TANK_BASE_LENGTH / 2, 0.0, -TANK_BASE_WIDTH / 2);
				glTexCoord2f(0.0, 0.0);
				glVertex3f(-TANK_BASE_LENGTH / 2, 0.0, TANK_BASE_WIDTH / 2);
				glTexCoord2f(1.0, 0.0);
				glVertex3f(-TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, TANK_BASE_WIDTH / 2);
				glTexCoord2f(1.0, 1.0);
				glVertex3f(-TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, -TANK_BASE_WIDTH / 2);
			glEnd();

			glBindTexture(GL_TEXTURE_2D, textures[4]);

			glBegin(GL_QUADS);
				glNormal3d(0, 1, 0);
				glTexCoord2f(0.0, 1.0);
				glVertex3f(-TANK_BASE_LENGTH / 2, 0.0, TANK_BASE_WIDTH / 2);
				glTexCoord2f(0.0, 0.0);
				glVertex3f(TANK_BASE_LENGTH / 2, 0.0, TANK_BASE_WIDTH / 2);
				glTexCoord2f(1.0, 0.0);
				glVertex3f(TANK_BASE_LENGTH / 2, 0.0, -TANK_BASE_WIDTH / 2);
				glTexCoord2f(1.0, 1.0);
				glVertex3f(-TANK_BASE_LENGTH / 2, 0.0, -TANK_BASE_WIDTH / 2);
			glEnd();

			glBegin(GL_QUADS);
				glNormal3d(0, -1, 0);
				glTexCoord2f(0.0, 1.0);
				glVertex3f(-TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, TANK_BASE_WIDTH / 2);
				glTexCoord2f(0.0, 0.0);
				glVertex3f(TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, TANK_BASE_WIDTH / 2);
				glTexCoord2f(1.0, 0.0);
				glVertex3f(TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, -TANK_BASE_WIDTH / 2);
				glTexCoord2f(1.0, 1.0);
				glVertex3f(-TANK_BASE_LENGTH / 2, TANK_BASE_HEIGHT, -TANK_BASE_WIDTH / 2);
			glEnd();

		glPopMatrix();

		glPushMatrix();		// glowa i lufa
			
			glTranslatef(newX, newY, newZ);

			glPushMatrix();	// glowa
				glTranslatef(0.0, TANK_BASE_HEIGHT, 0.0);
				glRotatef(newAngle, angleX, 0, angleZ);
				glRotatef(direction * 90, 0, 1, 0);		// obrot w odpowiednim kierunku

				glBindTexture(GL_TEXTURE_2D, textures[6]);

				glBegin(GL_QUADS);
					glNormal3d(0, 0, 1);
					glTexCoord2f(0.0, 0.0);
					glVertex3f(-TANK_HEAD_LENGTH / 2, 0.0, TANK_HEAD_WIDTH / 2);
					glTexCoord2f(1.0, 0.0);
					glVertex3f(TANK_HEAD_LENGTH / 2, 0.0, TANK_HEAD_WIDTH / 2);
					glTexCoord2f(1.0, 1.0);
					glVertex3f(TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, TANK_HEAD_WIDTH / 2);
					glTexCoord2f(0.0, 1.0);
					glVertex3f(-TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, TANK_HEAD_WIDTH / 2);
				glEnd();

				glBegin(GL_QUADS);
					glNormal3d(1, 0, 0);
					glTexCoord2f(0.0, 1.0);
					glVertex3f(TANK_HEAD_LENGTH / 2, 0.0, TANK_HEAD_WIDTH / 2);
					glTexCoord2f(0.0, 0.0);
					glVertex3f(TANK_HEAD_LENGTH / 2, 0.0, -TANK_HEAD_WIDTH / 2);
					glTexCoord2f(1.0, 0.0);
					glVertex3f(TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, -TANK_HEAD_WIDTH / 2);
					glTexCoord2f(1.0, 1.0);
					glVertex3f(TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, TANK_HEAD_WIDTH / 2);
				glEnd();

				glBegin(GL_QUADS);
					glNormal3d(0, 0, -1);
					glTexCoord2f(0.0, 1.0);
					glVertex3f(TANK_HEAD_LENGTH / 2, 0.0, -TANK_HEAD_WIDTH / 2);
					glTexCoord2f(0.0, 0.0);
					glVertex3f(-TANK_HEAD_LENGTH / 2, 0.0, -TANK_HEAD_WIDTH / 2);
					glTexCoord2f(1.0, 0.0);
					glVertex3f(-TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, -TANK_HEAD_WIDTH / 2);
					glTexCoord2f(1.0, 1.0);
					glVertex3f(TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, -TANK_HEAD_WIDTH / 2);
				glEnd();

				glBegin(GL_QUADS);
					glNormal3d(-1, 0, 0);
					glTexCoord2f(0.0, 1.0);
					glVertex3f(-TANK_HEAD_LENGTH / 2, 0.0, -TANK_HEAD_WIDTH / 2);
					glTexCoord2f(0.0, 0.0);
					glVertex3f(-TANK_HEAD_LENGTH / 2, 0.0, TANK_HEAD_WIDTH / 2);
					glTexCoord2f(1.0, 0.0);
					glVertex3f(-TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, TANK_HEAD_WIDTH / 2);
					glTexCoord2f(1.0, 1.0);
					glVertex3f(-TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, -TANK_HEAD_WIDTH / 2);
				glEnd();

				glBegin(GL_QUADS);
					glNormal3d(0, 1, 0);
					glTexCoord2f(0.0, 1.0);
					glVertex3f(-TANK_HEAD_LENGTH / 2, 0.0, TANK_HEAD_WIDTH / 2);
					glTexCoord2f(0.0, 0.0);
					glVertex3f(TANK_HEAD_LENGTH / 2, 0.0, TANK_HEAD_WIDTH / 2);
					glTexCoord2f(1.0, 0.0);
					glVertex3f(TANK_HEAD_LENGTH / 2, 0.0, -TANK_HEAD_WIDTH / 2);
					glTexCoord2f(1.0, 1.0);
					glVertex3f(-TANK_HEAD_LENGTH / 2, 0.0, -TANK_HEAD_WIDTH / 2);
				glEnd();

				glBindTexture(GL_TEXTURE_2D, textures[3]);

				glBegin(GL_QUADS);
					glNormal3d(0, -1, 0);
					glTexCoord2f(0.0, 1.0);
					glVertex3f(-TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, TANK_HEAD_WIDTH / 2);
					glTexCoord2f(0.0, 0.0);
					glVertex3f(TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, TANK_HEAD_WIDTH / 2);
					glTexCoord2f(1.0, 0.0);
					glVertex3f(TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, -TANK_HEAD_WIDTH / 2);
					glTexCoord2f(1.0, 1.0);
					glVertex3f(-TANK_HEAD_LENGTH / 2, TANK_HEAD_HEIGHT, -TANK_HEAD_WIDTH / 2);
				glEnd();

			glPopMatrix();

			glPushMatrix();	// lufa
				glTranslatef(0.0, TANK_BASE_HEIGHT + TANK_HEAD_HEIGHT / 2, 0.0);
				glRotatef(newAngle, angleX, 0, angleZ);
				glRotatef(direction * 90, 0, 1, 0);		// obrot w odpowiednim kierunku
				glTranslatef(TANK_HEAD_LENGTH / 2, 0.0, 0.0);

				glBindTexture(GL_TEXTURE_2D, textures[2]);

				glBegin(GL_QUADS);
					glNormal3d(0, 0, 1);
					glTexCoord2f(0.0, 0.0);
					glVertex3f(-TANK_CANNON_LENGTH / 2, 0.0, TANK_CANNON_WIDTH / 2);
					glTexCoord2f(1.0, 0.0);
					glVertex3f(TANK_CANNON_LENGTH / 2, 0.0, TANK_CANNON_WIDTH / 2);
					glTexCoord2f(1.0, 1.0);
					glVertex3f(TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, TANK_CANNON_WIDTH / 2);
					glTexCoord2f(0.0, 1.0);
					glVertex3f(-TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, TANK_CANNON_WIDTH / 2);
				glEnd();

				glBindTexture(GL_TEXTURE_2D, textures[7]);

				glBegin(GL_QUADS);
					glNormal3d(1, 0, 0);
					glTexCoord2f(0.0, 1.0);
					glVertex3f(TANK_CANNON_LENGTH / 2, 0.0, TANK_CANNON_WIDTH / 2);
					glTexCoord2f(0.0, 0.0);
					glVertex3f(TANK_CANNON_LENGTH / 2, 0.0, -TANK_CANNON_WIDTH / 2);
					glTexCoord2f(1.0, 0.0);
					glVertex3f(TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, -TANK_CANNON_WIDTH / 2);
					glTexCoord2f(1.0, 1.0);
					glVertex3f(TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, TANK_CANNON_WIDTH / 2);
				glEnd();

				glBindTexture(GL_TEXTURE_2D, textures[2]);

				glBegin(GL_QUADS);
					glNormal3d(0, 0, -1);
					glTexCoord2f(0.0, 1.0);
					glVertex3f(TANK_CANNON_LENGTH / 2, 0.0, -TANK_CANNON_WIDTH / 2);
					glTexCoord2f(0.0, 0.0);
					glVertex3f(-TANK_CANNON_LENGTH / 2, 0.0, -TANK_CANNON_WIDTH / 2);
					glTexCoord2f(1.0, 0.0);
					glVertex3f(-TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, -TANK_CANNON_WIDTH / 2);
					glTexCoord2f(1.0, 1.0);
					glVertex3f(TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, -TANK_CANNON_WIDTH / 2);
				glEnd();

				glBegin(GL_QUADS);
					glNormal3d(-1, 0, 0);
					glTexCoord2f(0.0, 1.0);
					glVertex3f(-TANK_CANNON_LENGTH / 2, 0.0, -TANK_CANNON_WIDTH / 2);
					glTexCoord2f(0.0, 0.0);
					glVertex3f(-TANK_CANNON_LENGTH / 2, 0.0, TANK_CANNON_WIDTH / 2);
					glTexCoord2f(1.0, 0.0);
					glVertex3f(-TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, TANK_CANNON_WIDTH / 2);
					glTexCoord2f(1.0, 1.0);
					glVertex3f(-TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, -TANK_CANNON_WIDTH / 2);
				glEnd();

				glBegin(GL_QUADS);
					glNormal3d(0, 1, 0);
					glTexCoord2f(0.0, 1.0);
					glVertex3f(-TANK_CANNON_LENGTH / 2, 0.0, TANK_CANNON_WIDTH / 2);
					glTexCoord2f(0.0, 0.0);
					glVertex3f(TANK_CANNON_LENGTH / 2, 0.0, TANK_CANNON_WIDTH / 2);
					glTexCoord2f(1.0, 0.0);
					glVertex3f(TANK_CANNON_LENGTH / 2, 0.0, -TANK_CANNON_WIDTH / 2);
					glTexCoord2f(1.0, 1.0);
					glVertex3f(-TANK_CANNON_LENGTH / 2, 0.0, -TANK_CANNON_WIDTH / 2);
				glEnd();

				glBegin(GL_QUADS);
					glNormal3d(0, -1, 0);
					glTexCoord2f(0.0, 1.0);
					glVertex3f(-TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, TANK_CANNON_WIDTH / 2);
					glTexCoord2f(0.0, 0.0);
					glVertex3f(TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, TANK_CANNON_WIDTH / 2);
					glTexCoord2f(1.0, 0.0);
					glVertex3f(TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, -TANK_CANNON_WIDTH / 2);
					glTexCoord2f(1.0, 1.0);
					glVertex3f(-TANK_CANNON_LENGTH / 2, TANK_CANNON_HEIGHT, -TANK_CANNON_WIDTH / 2);
				glEnd();

			glPopMatrix();

		glPopMatrix();

	glPopMatrix();

	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);
}

void Tank::erase()
{
	for (unsigned int i = 0; i < tanks.size(); i++)	// szukamy czolgu ktory powinien zniknac
	{
		if (tanks[i].getHp() <= 0)
		{
			tanks.erase(tanks.begin() + i);
			break;
		}
	}
}