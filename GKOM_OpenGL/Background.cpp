#include "Background.h"


Background::Background()
{
	width = BACKGROUND_WIDTH;
	height = BACKGROUND_HEIGHT;
}


Background::~Background()
{
}

void Background::draw()
{
	GLfloat background_diffuse[] = { 0.5, 0.5, 0.5, 1.0 };
	GLfloat background_ambient[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat background_specular[] = { 0.8, 0.8, 0.8, 1.0 };

	glMaterialfv(GL_FRONT, GL_DIFFUSE, background_diffuse);
	glMaterialfv(GL_FRONT, GL_AMBIENT, background_ambient);

	glEnable(GL_TEXTURE_2D);
	glPushMatrix();
		glBindTexture(GL_TEXTURE_2D, textures[10]);

		glBegin(GL_QUADS);
			glTexCoord2f(1.0, 0.0);
			glVertex3f(-width / 2, 0.0, height / 2);
			glTexCoord2f(0.0, 0.0);
			glVertex3f(width / 2, 0.0, height / 2);
			glTexCoord2f(0.0, 1.0);
			glVertex3f(width / 2, 0.0, -height / 2);
			glTexCoord2f(1.0, 1.0);
			glVertex3f(-width / 2, 0.0, -height / 2);
		glEnd();
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
}